const scores = [60, 50, 60, 58, 54, 54,
    58, 50, 52, 54, 48, 69,
    34, 55, 51, 52, 44, 51,
    69, 64, 66, 55, 52, 61,
    46, 31, 57, 52, 44, 18,
    41, 53, 55, 61, 51, 44];

const costs = [.25, .27, .25, .25, .25, .25,
    .33, .31, .25, .29, .27, .22,
    .31, .25, .25, .50, .21, .25,
    .25, .27, .25, .25, .25, .25,
    .33, .31, .25, .29, .27, .22,
    .31, .25, .25, .33, .21, .29];

function printAndGetHighScores(scores) {
    let highScore = 0;
    let output;

    for (let i = 0; i < scores.length; i++) {
        output = "Płyn do baniek nr " + i + " wynik: " + scores[i];
        console.log(output);

        if (scores[i] > highScore) {
            highScore = scores[i];
        }
    }
    return highScore;
}

function getBestSolution(highScore, scores) {
    const bestSolution = [];

    for (let i = 0; i < scores.length; i++) {
        if (scores[i] === highScore) {
            bestSolution.push(i);
        }
    }

    return bestSolution;
}

function getMostCostEffectiveSolution(scores, costs) {
    let cost = 100;
    let index;

    for (let i = 0; i < scores.length; i++) {
        if (scores.length === costs.length) {
            scores[i] = costs[i];

            if (cost > costs[i]) {
                index = i;
                cost = costs[i];
            }
        }
    }
    return index;
}


const highScore = printAndGetHighScores(scores);
console.log("Liczba testów: " + scores.length);
console.log("Największa liczba wytworzonych baniek: " + highScore);
console.log('');

const bestSolution = getBestSolution(highScore, scores);
console.log("Płyny z najlepszym wynikiem: " + bestSolution);
console.log('');

const mostCostEffective =  getMostCostEffectiveSolution(scores, costs);
console.log("Płyn number " + mostCostEffective + " jest najbardziej opłacalny.");
console.log('');


