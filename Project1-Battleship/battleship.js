var randomLoc = Math.floor(Math.random() * 5);

var guess = ''; //przechowuje wartość położenia do sprawdzenia
var hits = 0; //przechowuje liczbę trafień
var guesses = 0; //przechowuje liczbę prób
var isSunk = false;
var guessedPositions = [];


function alreadyGuessed(userGuess) {
    for (var i = 0; i < guessedPositions.length; i++) {
        if (guessedPositions[i] === userGuess) {
            return true;
        }
    }
    return false;
}

function init({
                  location1,
                  location2,
                  location3,
              }) {

    console.log('location1', location1);
    console.log('location2', location2);
    console.log('location3', location3);

    while (isSunk === false) {
        guess = prompt("Ready, steady, GO! Enter a number between 0-6 :");
        const numberGuess = Number.parseInt(guess); // drugi sposób -> +guess

        if (guess < 0 || guess > 6) {
            alert("You can enter only number between 0 - 6 ");
        } else if (alreadyGuessed(guess)) {
            alert("You have already guessed " + guess + ", please enter a different location");
        } else {
            guesses = guesses + 1;
            guessedPositions.push(guess);

            const isConditional = numberGuess === location1 || numberGuess === location2 || numberGuess === location3;

            if (isConditional) {
                alert("HIT");
                hits = hits + 1;

                if (hits === 3) {
                    isSunk = true;
                    alert("You sank my Examples!");
                }
            } else {
                alert("MISSED")
            }
        }
    }

};

function statistic() {
    var stats = "You needed " + guesses + " try to sink my ship!" + " Your efficiency is : " + (hits / guesses) + ".";
    alert(stats);
};


init({
    location1: randomLoc,
    location2: this.location1 + 1,
    location3: this.location2 + 1
});
statistic();






