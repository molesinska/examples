const init = require('./.');

window.prompt = jest.fn();
window.alert = jest.fn();

describe('Examples', () => {
    test('alert verification to hit and sank Examples', () => {

        window.prompt.mockImplementationOnce(() => 2).mockImplementationOnce(() => 3).mockImplementationOnce(() => 4);
        init({ location1: 2,
            location2: 3,
            location3: 4});

        expect(window.alert).toBeCalledWith('HIT');
        expect(window.alert).toBeCalledWith('HIT');
        expect(window.alert).toBeCalledWith('HIT');
        expect(window.alert).toBeCalledWith("You sank my Examples!");
    });
});


